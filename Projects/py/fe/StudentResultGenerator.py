class Student:
    def __init__(self, marks_list):
        self.marks_list = marks_list

    def compute_grade(self):
        total_marks = 0
        for marks in self.marks_list:
            if marks < 40:
                return "fail"
            total_marks += marks

        aggregate = total_marks / 5
        if aggregate > 75:
            return "distinction"
        elif aggregate >= 60:
            return "first division"
        elif aggregate >= 50:
            return "second division"

        return "third division"


def main():
    marks_list = list()
    subject_count = 5

    for index in range(0, subject_count):
        score = int(input("Enter marks in subject {0}:".format(str(index + 1))))
        marks_list.append(score)

    student = Student(marks_list)
    print("Result:", student.compute_grade())


if __name__ == '__main__':
    main()
