import sys

def main():
    N = int(input("Enter value of N:"))
    list_of_numbers  = list();
    for i in range(0,N):
        num = float(input("Enter a number:"))
        list_of_numbers.append(num)

    print("Maximum number in the list:" , find_maximum(list_of_numbers) , find_maximum_loop(list_of_numbers))
    print("Minimum number in the list:" , find_minimum(list_of_numbers) , find_minimum_loop(list_of_numbers))
    sum_avg = find_sum_average(list_of_numbers)
    print("Sum of numbers in the list:" , sum_avg[0])
    print("Average of numbers in the list:" , sum_avg[1])



def find_maximum(list_of_numbers):
    max_val = max(list_of_numbers)
    return max_val

def find_maximum_loop(list_of_numbers):
     max_val = sys.float_info.min
     for num in list_of_numbers:
         if(num > max_val):
             max_val = num

     return max_val;

def find_minimum(list_of_numbers):
    min_val = min(list_of_numbers)
    return min_val

def find_minimum_loop(list_of_numbers):
     min_val = sys.float_info.max
     for num in list_of_numbers:
         if(num < min_val):
             min_val = num

     return min_val;



def find_sum_average(list_of_numbers):
    sum_num = sum(list_of_numbers)
    avg = sum_num/(len(list_of_numbers))
    return (sum_num,avg)


if __name__ == '__main__':
  main();