import math


class Compute:

    def __init__(self, input_number):
        self.num = input_number

    def compute_square(self):
        return self.num * self.num

    def compute_squareroot(self):
        return math.sqrt(self.num)

    def compute_cube(self):
        return math.pow(self.num, 3)

    def is_prime(self):
        if self.num < 2:
            return False
        limit = int(math.sqrt(self.num))

        for divisor in range(2, limit + 1):
            if self.num % divisor == 0:
                return False

        return True

    def compute_factorial(self):

        result = 1
        for values in range(2, self.num + 1):
            result *= values

        return result

    def compute_prime_factors(self):
        prime_factors = set()
        old_num = self.num
        for i in range(2, self.num + 1):
            self.num = i
            if self.is_prime() and old_num % i == 0:
                prime_factors.add(i)

        return prime_factors


if __name__ == '__main__':
    compute = Compute(int(input("Enter a natural number:")))

    print("Square root:", compute.compute_squareroot())
    print("Square:", compute.compute_square())
    print("Cube:", compute.compute_cube())
    print("Is number prime:", compute.is_prime())
    print("Factorial:", compute.compute_factorial())
    print("Prime factors:", compute.compute_prime_factors())
