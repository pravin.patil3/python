def print_digits_in_reverse(num):
    q = num
    while q > 0:
        t = int(q / 10)
        r = q % 10
        q = t
        print(r, end=" ")


print_digits_in_reverse(int(input("Enter a positive number:")))
