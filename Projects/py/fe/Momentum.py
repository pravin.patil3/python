class Object:
    def __init__(self , mass , velocity):
        self.mass = mass
        self.velocity = velocity

    def calculate_momentum(self):
        return self.mass * self.velocity * self.velocity


def main():
    mass = int(input("Enter mass in kg:"))
    velocity = int(input("Enter velocity in m/s:"))
    object = Object(mass,velocity)
    print("Momentum:",object.calculate_momentum())

if __name__ == '__main__':
    main()
